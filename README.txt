This is a simple module for managing ftp users with a mysql enabled pureftp service.
More on pureftp here: http://www.pureftpd.org
More on configuring it with mysql here: http://www.pureftpd.org/README.MySQL

Included in this directory is a sample pureftp configuration file, pureftp-mysql.conf,
as well as the ubiquitous .mysql file required for installation.

Send comments to welch@advomatic.com

Originally developed for advomatic.com by Aaron Welch (crunchywelch) at Advomatic LLC
